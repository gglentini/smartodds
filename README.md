Python/Django Developer Test
=====================================

A Django project called **smartodds**, which contains an application called **tennis**.

The **tennis** app should provide functionality for the following:

* Storing historical results.
* Manual editing of data.
* Fetching and populating the database with data from http://tennis-data.co.uk (the 2011 file is sufficient http://tennis-data.co.uk/2011/2011.zip).