"""
Settings to register tennis app to the Django admin module

"""

from smartodds.tennis.models import Player, Tournament, Match, MatchPlayerStat
from django.contrib import admin

admin.site.register(Player)
admin.site.register(Tournament)
admin.site.register(Match)
admin.site.register(MatchPlayerStat)
