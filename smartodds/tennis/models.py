from django.db import models

# Create your models here.

from django.db import models

# Model storing the main information about an ATP tournament
class Tournament(models.Model):
	atp = models.IntegerField('ATP')
	start_date = models.DateField('Start Date', null=True)
	tournament = models.CharField('Tournament', null=True, max_length=255)
	venue = models.CharField('Venue', max_length=255)
	location = models.CharField('Location', max_length=255)
	series = models.CharField('Series', max_length=255)
	court = models.CharField('Court', max_length=255)
	surface = models.CharField('Surface', max_length=255)
	players = models.IntegerField('Players', null=True)
	
	def __unicode__(self):
		# some tournaments has the full info in 'tournament' field, some not...
		return self.tournament or self.series
	
# Model storing the information about a match in a tournament
class Match(models.Model):
	tournament = models.ForeignKey('Tournament')
	series_tag = models.CharField(max_length=255)
	date = models.DateField()
	round = models.CharField(max_length=255)
	best_of = models.IntegerField()
	comment = models.CharField(max_length=255)
	winner = models.ForeignKey('MatchPlayerStat', related_name='winner')
	loser = models.ForeignKey('MatchPlayerStat', related_name='loser')
	
	def __unicode__(self):
		return self.winner.player.__unicode__() + " VS " + self.loser.player.__unicode__() + " @ " + self.tournament.__unicode__()
	
class Player(models.Model):
	name = models.CharField(max_length=255)
# 	surname = models.CharField(max_length=255)
# 	age = models.IntegerField()
	
	def __unicode__(self):
# 		return self.name[0].capitalize()+". "+self.surname.capitalize()
		return self.name

# Model to store the information about a player who has played a match
class MatchPlayerStat(models.Model):
	player = models.ForeignKey('Player')
	# next two field are CharField because in same case we have strings like "N/A"
	rank = models.CharField(max_length=255)
	points = models.CharField(max_length=255)
	games_won_1st_set = models.IntegerField(null=True)
	games_won_2nd_set = models.IntegerField(null=True)
	games_won_3rd_set = models.IntegerField(null=True)
	games_won_4th_set = models.IntegerField(null=True)
	games_won_5th_set = models.IntegerField(null=True)
	sets_won = models.IntegerField(null=True)
	b365 = models.FloatField('bet365 odds', null=True)
	ex = models.FloatField('expekt odds', null=True)
	lb = models.FloatField('labrokes odds', null=True)
	ps = models.FloatField('pinnacles odds', null=True)
	sj = models.FloatField('stanjames odds',null=True)
# 	bw = models.DecimalField('bet&win odds', null=True, max_digits=3, decimal_places=2)
# 	cb = models.DecimalField('centrebet odds', null=True, max_digits=3, decimal_places=2)
# 	gb = models.DecimalField('gamebookers odds', null=True, max_digits=3, decimal_places=2)
# 	iw = models.DecimalField('interwetten odds', null=True, max_digits=3, decimal_places=2)
# 	sb = models.DecimalField('sportingbet odds', null=True, max_digits=3, decimal_places=2)
# 	ub = models.DecimalField('unibet odds', null=True, max_digits=3, decimal_places=2)
	max_odds = models.FloatField(null=True)
	avg_odds = models.FloatField(null=True)
	
	def __unicode__(self):
		return "Stats %s for %s" % (self.id, self.player)
	