from django import forms

class ImportForm(forms.Form):
    filename = forms.CharField(max_length=255)

