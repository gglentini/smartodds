# Create your views here.

from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render_to_response, render
from django.template import RequestContext

from smartodds import settings
from smartodds.tennis.models import Tournament, Player, MatchPlayerStat, Match
from smartodds.tennis.tennis_forms import ImportForm

import xlrd


int_keys = ["Wsets", "Lsets", "W1", "W2", "W3", "W4", "W5", "L1", "L2", "L3", "L4", "L5"]
float_keys =["B365W", "EXW", "LBW", "PSW", "SJW", "MaxW", "AvgW", "B365L", "EXL", "LBL", "PSL", "SJL", "MaxL", "AvgL"] 
	

def _format_date(date_xls):
	"""Utility function to properly use the DateTime format required by Django field."""
	tuple_date = xlrd.xldate_as_tuple(date_xls,0)
	return "%s-%s-%s" % (tuple_date[0], tuple_date[1], tuple_date[2])

def _import_tournaments(sheet):
	"""Utility function to import tournaments."""
	tournaments = []
	#first row contains the labels
	keys = sheet.row_values(0)
	# saving each row in a dict, using labels as keys, and add it a list
	for rownum in range(1,sheet.nrows):
		values = sheet.row_values(rownum)
		t = dict(zip(keys, values))

		Tournament.objects.get_or_create(atp=t.get('ATP'),
										start_date = _format_date(t.get('Start Date')),
										court = t.get('Court'),
										tournament = t.get('Tournament'),
										series = t.get('Series'),
										venue = t.get('Venue'),
										location = t.get('Location'),
										surface = t.get('Surface'),
										players = t.get('Players'),
										)
		
def _validate_data(data):
	"""Utility function to process integer and float values,
	in order to be safely added in the database. """
	
	for k in int_keys:
		try:
			int(data.get(k))
		except ValueError:
			data[k] = None
	for k in float_keys:
		try:
			float(data.get(k))
		except ValueError:
			data[k] = None
		
def _import_player_stats(data):
	"""Utility function to import the statistics about the players of a match. 
	"""
	
	# create the object for the two players
	winner_db, _created = Player.objects.get_or_create(name=data.get("Winner"))
	loser_db, _created = Player.objects.get_or_create(name=data.get("Loser"))

		
	# get or create the object containing the winner statistics	
	winner_stats_db, _created = MatchPlayerStat.objects.get_or_create(player=winner_db,
															rank = data.get("WRank"),
															points = data.get("WPts"),
															sets_won = data.get("Wsets"),
															games_won_1st_set=data.get("W1"),
															games_won_2nd_set=data.get("W2"),
															games_won_3rd_set=data.get("W3"),
															games_won_4th_set=data.get("W4"),
															games_won_5th_set=data.get("W5"),
															b365=(data.get("B365W")),
															ex=(data.get("EXW")),
															lb=(data.get("LBW")),
															ps=(data.get("PSW")),
															sj=(data.get("SJW")),
															max_odds=(data.get("MaxW")),
															avg_odds=(data.get("AvgW"))
															)
															
	# get or create the object containing the loser statistics							
	loser_stats_db, _created = MatchPlayerStat.objects.get_or_create(player=loser_db,
															rank = data.get("LRank"),
															points = data.get("LPts"),
															sets_won = data.get("Lsets"),
															games_won_1st_set=data.get("L1"),
															games_won_2nd_set=data.get("L2"),
															games_won_3rd_set=data.get("L3"),
															games_won_4th_set=data.get("L4"),
															games_won_5th_set=data.get("L5"),
															b365=(data.get("B365L")),
															ex=(data.get("EXL")),
															lb=(data.get("LBL")),
															ps=(data.get("PSL")),
															sj=(data.get("SJL")),
															max_odds=(data.get("MaxL")),
															avg_odds=(data.get("AvgL"))
															)
															
	return winner_stats_db, loser_stats_db
	

def _import_matches(sheet):
	"""Utility function to import matches."""
	matches = []
	keys = sheet.row_values(0)
	for rownum in range(1,sheet.nrows):
		# put match info a dict	
		values = sheet.row_values(rownum)
		m = dict(zip(keys, values))
		
		# change statistics values taken from the xls in order to be easily 
		# saved as IntegerField or FloatField in the database.
		# TODO: An option to avoid such a validation here could be to handle every field as a string
		_validate_data(m)
		
		# import winner and loser statistics
		winner_stats_db, loser_stats_db = _import_player_stats(m)
	  	
	  	# tournaments in match sheet are not in the tournament one, or the ATP are not unique
	  	# so get_or_create here the tournament if it does not exist in the table
	  	tournament_db, _created = Tournament.objects.get_or_create(atp=m.get("ATP"),
	  													court = m.get('Court'),
														tournament = m.get('Tournament'),
														series = m.get('Series'),
														location = m.get('Location'),
														surface = m.get('Surface'),
	  													)
	  	
	  	# import the match data
	  	Match.objects.get_or_create(winner=winner_stats_db, loser=loser_stats_db,
	  							 	tournament = tournament_db,
	  							 	series_tag = m.get("Series"),
	  							 	date = _format_date(m.get("Date")),
	  							 	round = m.get("Round"),
	  							 	best_of = m.get("Best of"),
	  							 	comment = m.get("Comment")
	  								)

def import_from_file(request):
	"""Imports data from an xls file to the application DB.
	"""
	exit_mssg = None
	try:
		# open the xls file
		if request.POST.get("filename"):
			xls = request.POST.get("filename")
		else:
			xls = settings.STATIC_ROOT+'2011.xls'
		
		wb = xlrd.open_workbook(xls)
		
		# get tournaments information from 1st sheet
		# skip importing 1st sheet if we don't have 1+ sheets, 2013 odds has only 1 sheet...
		sh0 = wb.sheet_by_index(0)
		if wb.nsheets > 1:
			_import_tournaments(sh0)
			_import_matches(wb.sheet_by_index(1))
		else:
			_import_matches(sh0)
		exit_mssg = 'Data successfully imported.'
	except Exception, exc:
		exit_mssg = str(exc)
	finally:
		return exit_mssg
# 	return render(request, 'tennis/import.html', {'form': form}, context_instance=RequestContext(request))
	
def import_page(request):
	"""Entry view to import data from a file.
	
	If invoked through a GET (http://localhost:8000/tennis/import) a form is displayed,
	and the user could insert the xls source file.
	
	When the form is submitted, the view is called using a POST, and the data will be
	imported from the source file to the database.
	
	The result of the operation is then displayed above the form.
	"""
	mssg = None
	if request.method == 'POST': # If the form has been submitted...
		form = ImportForm(request.POST) # A form bound to the POST data
		if form.is_valid(): # All validation rules pass
			mssg = import_from_file(request)
	else:
		form = ImportForm() # An unbound form

	return render(request, 'tennis/import.html', {'form': form, 'result':mssg}, context_instance=RequestContext(request))

def home(request):
	return render_to_response('tennis/home.html', {},)