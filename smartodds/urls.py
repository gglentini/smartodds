from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
	url(r'^$', 'smartodds.views.home'),
    # url(r'^smartodds/', include('smartodds.foo.urls')),
    url(r'^tennis/$', 'smartodds.tennis.views.home'),
	url(r'^tennis/import/', 'smartodds.tennis.views.import_page'),
	
    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
)
